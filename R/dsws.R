#' @export
#' @import dsws
#' @import memoise
#' @import cachem
#' 
#' @inherit dsws::dsws_connect
dsws_connect <- dsws::dsws_connect

#' Memoised version of \code{\link[dsws]{dsws_static}}
#'
#' @export
#' @import dsws
#' @import memoise
#' @import cachem
#' 
#' @inheritParams dsws::dsws_static
dsws_static <- dsws::dsws_static

#' Memoised version of \code{\link[dsws]{dsws_timeseries}}
#'
#' @export
#' @import dsws
#' @import memoise
#' @import cachem
#' 
#' @inheritParams dsws::dsws_timeseries
dsws_timeseries <- dsws::dsws_timeseries
